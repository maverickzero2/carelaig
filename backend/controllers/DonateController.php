<?php

namespace backend\controllers;

use Yii;
use app\models\Donate;
use app\models\DonateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DonateController implements the CRUD actions for Donate model.
 */
class DonateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Donate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DonateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKonfirmasi($id,$status,$prev_status)
    {
      $model = $this->findModel($id);
      $projectmodel = new \app\models\Project();
      $project = $projectmodel->findOne($id);
      $curr_progress=$projectmodel->progress_target;
      if($status=='1')
      {
        $model->status=1;
        $curr_progress+=$model->amount;
        Yii::$app->db->createCommand('UPDATE project SET progress_target=:progress_target WHERE id=:id')
        ->bindParam(':progress_target',$curr_progress)
        ->bindParam(':id',$id)
        ->execute();
      }
      else if($status=='2'){
        $model->status=2;
        if($prev_status==1)
        {
          $curr_progress-=$model->amount;
          Yii::$app->db->createCommand('UPDATE project SET progress_target=:progress_target WHERE id=:id')
          ->bindParam(':progress_target',$curr_progress)
          ->bindParam(':id',$id)
          ->execute();
        }
      }
      $model->save();
      return $this->redirect(['index']);
    }

    /**
     * Displays a single Donate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Donate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Donate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Donate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Donate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Donate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Donate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Donate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
