<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Komunitasmember */

$this->title = 'Create Komunitasmember';
$this->params['breadcrumbs'][] = ['label' => 'Komunitasmembers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitasmember-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
