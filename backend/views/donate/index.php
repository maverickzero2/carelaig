<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\icons\Icon;
Icon::map($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\DonateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Donasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'label'=>'Project',
              'format'=>'raw',
              'attribute'=>'project',
              'value'=>'project.name',
            ],
            [
              'label'=>'User',
              'format'=>'raw',
              'attribute'=>'user',
              'value'=>'user.name',
            ],
            [
              'label'=>'Nama',
              'format'=>'raw',
              'attribute'=>'nama',
              'value'=>function($dataProvider){
                if($dataProvider->nama==null)
                {
                  return '-';
                }else{
                  return $dataProvider->nama;
                }
              }
            ],
            [
              'label'=>'No Rekening/Alamat',
              'value'=>'rekening',
            ],
            [
              'label'=>'Jumlah Donasi',
              'value'=>'amount',
            ],
            [
              'label'=>'Status',
              'format'=>'raw',
              'attribute'=>'status',
              'value'=>function($dataProvider){
                if($dataProvider->status==0)
                {
                  return 'wait';
                }else if($dataProvider->status==1){
                  return Icon::show('check');
                }else{
                  return Icon::show('times');
                }
              }
            ],
            [
              'label'=>'Jumlah Donasi',
              'value'=>'amount',
            ],
            [
              'label'=>'Tanggal',
              'format'=>'raw',
              'attribute'=>'date',
              'value'=>function($dataProvider){
                return date('Y-m-d',strtotime($dataProvider->date));
              }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{approve} {reject}',
                'header'=>'Action',
                'buttons' => [
                   'approve' => function ($url, $model) {
                      $url = Url::to(['konfirmasi', 'id' => $model->id, 'status'=>'1','prev_status'=>$model->status]);
                      $class='';
                      if($model->status==1)
                      {
                        $class='btn btn-success disabled';
                      }else{
                        $class='btn btn-success';
                      }
                      return Html::a(Icon::show('check'), $url, ['title' => 'approve','class'=>$class]);
                   },
                   'reject' => function ($url, $model) {
                       $url = Url::to(['konfirmasi', 'id' => $model->id, 'status'=>'2','prev_status'=>$model->status]);
                       $class='';
                       if($model->status==2)
                       {
                         $class='btn btn-danger disabled';
                       }else{
                         $class='btn btn-danger';
                       }
                       return Html::a(Icon::show('times'), $url, ['title' => 'reject','class'=>$class]);
                   },
                ]
            ]
        ],
    ]); ?>
</div>
