<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $name
 * @property string $foto
 * @property string $deskripsi
 * @property string $lokasi
 * @property string $lat
 * @property string $lng
 * @property int $id_target
 * @property string $val_target
 * @property string $progress_target
 * @property string $date_start
 * @property string $date_end
 * @property string $created_date
 * @property int $id_komunitas
 * @property int $id_admin_komunitas
 *
 * @property Donate[] $donates
 * @property Komunitas $komunitas
 * @property User $adminKomunitas
 * @property Target $target
 * @property Volunteer[] $volunteers
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'deskripsi', 'id_target', 'val_target', 'date_start', 'date_end', 'id_komunitas', 'id_admin_komunitas'], 'required'],
            [['name', 'foto', 'deskripsi', 'lokasi'], 'string'],
            [['id_target', 'id_komunitas', 'id_admin_komunitas'], 'integer'],
            [['date_start', 'date_end', 'created_date'], 'safe'],
            [['lat', 'lng'], 'string', 'max' => 20],
            [['val_target', 'progress_target'], 'string', 'max' => 11],
            [['id_komunitas'], 'exist', 'skipOnError' => true, 'targetClass' => Komunitas::className(), 'targetAttribute' => ['id_komunitas' => 'id']],
            [['id_admin_komunitas'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_admin_komunitas' => 'id']],
            [['id_target'], 'exist', 'skipOnError' => true, 'targetClass' => Target::className(), 'targetAttribute' => ['id_target' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'foto' => 'Foto',
            'deskripsi' => 'Deskripsi',
            'lokasi' => 'Lokasi',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'id_target' => 'Id Target',
            'val_target' => 'Val Target',
            'progress_target' => 'Progress Target',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'created_date' => 'Created Date',
            'id_komunitas' => 'Id Komunitas',
            'id_admin_komunitas' => 'Id Admin Komunitas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonates()
    {
        return $this->hasMany(Donate::className(), ['id_project' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomunitas()
    {
        return $this->hasOne(Komunitas::className(), ['id' => 'id_komunitas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminKomunitas()
    {
        return $this->hasOne(User::className(), ['id' => 'id_admin_komunitas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(Target::className(), ['id' => 'id_target']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteers()
    {
        return $this->hasMany(Volunteer::className(), ['id_project' => 'id']);
    }
}
