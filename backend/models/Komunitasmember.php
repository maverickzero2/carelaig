<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "komunitasmember".
 *
 * @property int $id
 * @property int $id_komunitas
 * @property int $id_user
 * @property int $status
 * @property int $role
 * @property string $timestamp
 *
 * @property Komunitas $komunitas
 * @property User $user
 */
class Komunitasmember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'komunitasmember';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_komunitas', 'id_user'], 'required'],
            [['id_komunitas', 'id_user', 'status', 'role'], 'integer'],
            [['timestamp'], 'safe'],
            [['id_komunitas'], 'exist', 'skipOnError' => true, 'targetClass' => Komunitas::className(), 'targetAttribute' => ['id_komunitas' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_komunitas' => 'Id Komunitas',
            'id_user' => 'Id User',
            'status' => 'Status',
            'role' => 'Role',
            'timestamp' => 'Timestamp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomunitas()
    {
        return $this->hasOne(Komunitas::className(), ['id' => 'id_komunitas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
