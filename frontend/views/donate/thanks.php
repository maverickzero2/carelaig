<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Donate */

$this->title = 'Thanks';
$this->params['breadcrumbs'][] = ['label' => 'Thanks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donate-create">
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <div style="padding:20px">
          <h1>Terimakasih Telah Melakukan Donasi</h1>
          <h4>Konfirmasi Donasi akan Dikirimkan Melalui Email </h4>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
  $this->registerCssFile("@web/css/carelaig.css",[
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
  ], 'css-print-theme');
 ?>
