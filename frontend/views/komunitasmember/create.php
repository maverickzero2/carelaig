<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Komunitasmember */

$this->title = 'Create Komunitasmember';
$this->params['breadcrumbs'][] = ['label' => 'Komunitasmembers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitasmember-create">

    <h1>Jadi Member Komunitas</h1>
    Apakah anda yakin ingin menjadi member komunitas?
    <?= $this->render('_form', [
        'model' => $model,
        'id_komunitas' => $id_komunitas,
    ]) ?>

</div>
