<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KomunitasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thanks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitas-index">
  <h4>Anda telah melakukan permintaan gabung Komunitas</h4>
  <p>Konfirmasi akan diberitahukan melalui email</p>
</div>
<?php
$this->registerCssFile("@web/css/carelaig.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'css-print-theme');
?>
