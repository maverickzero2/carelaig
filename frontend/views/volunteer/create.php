<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Volunteer */

$this->title = 'Create Volunteer';
$this->params['breadcrumbs'][] = ['label' => 'Volunteers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="volunteer-create">

    <h1>Jadi Relawan</h1>
    <h4>Apakah anda yakin ingin mendaftar sebagai relawan?</h4>
    <?= $this->render('_form', [
        'model' => $model,
        'id_project'=>$id_project,
    ]) ?>

</div>
