<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Volunteer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="volunteer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $id = Yii::$app->user->identity->id;

    echo $form->field($model, 'id_project')
      ->hiddenInput(['value' => $id_project])
      ->label(false);

    echo $form->field($model, 'id_user')
      ->hiddenInput(['value' => $id])
      ->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Ya, Saya Bersedia', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Tidak', ['/site'], ['class'=>'btn btn-alert']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
