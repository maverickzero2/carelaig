<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Komunitas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="komunitas-form">
    <div class="row">
      <div class="col-lg-8 boxform">
        <h1><p class="text-center">Form Register Komunitas</p></h1>
        <?php $form = ActiveForm::begin(
          ['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?= $form->field($model, 'nama')->textInput(['placeholder'=>'Input Nama Komunitas'])->label(false) ?>

        <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6,'placeholder'=>'Deskripsi Komunitas'])->label(false) ?>

        <?= $form->field($model, 'foto')->fileInput(['class'=>'form form-inline']) ?>

        <?= $form->field($model, 'contact')->textInput(['placeholder'=>'Nomor Telp/Hp'])->label(false) ?>

        <?= $form->field($model, 'address')->textarea(['rows' => 3,'placeholder'=>'Alamat'])->label(false) ?>

        <?php echo $form->field($model, 'id_admin_komunitas')
          ->hiddenInput(['value' => Yii::$app->user->identity->id])
          ->label(false);
          ?>

        <div class="form-group">
            <?= Html::submitButton('Register', ['class' => 'btn btn-success buttonfull']) ?>
            <p class="text-center"><small>Dengan melakukan register berarti anda menyetujui<br>terms and condition carelaig</small></p>
        </div>

        <?php ActiveForm::end(); ?>
      </div>

    </div>


</div>
