<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KomunitasmemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Broadcast Message ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitasmember-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
      <div class="col-lg-8 boxform">
        <?php $form = ActiveForm::begin(
          ['options' => ['enctype' => 'multipart/form-data'],
              'method' => 'get',
              'action' => ['sendbroadcast', 'id_komunitas' => $id_komunitas]
          ]) ?>
        <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6,'placeholder'=>'Broadcast'])->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton('Broadcast', ['class' => 'btn btn-success buttonfull']) ?>
        </div>
        <?php ActiveForm::end(); ?>
      </div>
    </div>
</div>
