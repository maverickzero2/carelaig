<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KomunitasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Komunitas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitas-index">
  <h4>List Komunitas</h4>
  <table class="table table-bordered">
    <tr>
      <th>No</th>
      <th>Nama Komunitas</th>
      <th>Action</th>
    </tr>
    <?php
      $no=1;
      foreach($model as $models)
      {
        echo '<tr>';
        echo '<td>'.$no.'</td>';
        echo '<td>'.$models['nama'].'</td>';
        echo '<td>'.Html::a('Manage', ['managekomunitas', 'id_komunitas' => $models['id'],'id_user'=>Yii::$app->user->identity->id], ['class' => 'btn btn-primary']).'</td>';
        echo '</tr>';
        $no++;
      }
    ?>
</table>

</div>
<?php
$this->registerCssFile("@web/css/carelaig.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'css-print-theme');
?>
