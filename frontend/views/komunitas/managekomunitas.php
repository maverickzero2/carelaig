<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KomunitasmemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Komunitas '.$model->nama;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitasmember-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::a('Broadcast Message',
      Url::to(['broadcast','id_komunitas'=>$id_komunitas])
    , ['title' => 'broadcast','class'=>'btn btn-success']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute'=>'namaUser',
              'label'=>'Nama Member',
            ],
            [
              'attribute'=>'timestamp',
              'label'=>'Request Join Date',
              'value'=>function ($model, $index)
                      {
                        return date('Y-m-d',strtotime($model->timestamp));
                      }
            ],
            [
              'attribute'=>'status',
              'label'=>'Status',
              'value'=>function ($model, $index)
                      {
                        $label='Request Join';
                        if($model->status==1)
                        {
                          $label='Joined';
                        }
                        else if($model->status==2)
                        {
                          $label='Rejected';
                        }
                        return $label;
                      }
            ],
            [
              'attribute'=>'role',
              'label'=>'Role',
              'value'=>function ($model, $index)
                      {
                        $label='Non Member';
                        if($model->role==1)
                        {
                          $label='Member';
                        }
                        else if($model->role==2)
                        {
                          $label='Admin';
                        }
                        return $label;
                      }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{approve} {reject} {setadmin}',
                'header'=>'Action',
                'buttons' => [
                   'approve' => function ($url, $model) {
                      $url = Url::to(['konfirmasi', 'id' => $model->id,'status'=>1, 'id_komunitas'=>$model->id_komunitas,'id_user'=>Yii::$app->user->identity->id]);
                      $class='';
                      if($model->status==1)
                      {
                        $class='btn btn-success disabled';
                      }else{
                        $class='btn btn-success';
                      }
                      return Html::a('<span class="fa fa-check"></span>', $url, ['title' => 'approve','class'=>$class]);
                   },
                   'reject' => function ($url, $model) {
                       $url = Url::to(['konfirmasi', 'id' => $model->id,'status'=>2, 'id_komunitas'=>$model->id_komunitas,'id_user'=>Yii::$app->user->identity->id]);
                       $class='';
                       if($model->status==2)
                       {
                         $class='btn btn-danger disabled';
                       }else{
                         $class='btn btn-danger';
                       }
                       return Html::a('<span class="fa fa-times"></span>', $url, ['title' => 'reject','class'=>$class]);
                   },
                   'setadmin' => function ($url, $model) {
                        $class='btn btn-primary';
                        if($model->status==2)
                        {
                          $class='btn btn-primary disabled';
                        }
                       $url = Url::to(['setadmin', 'id' => $model->id,'status'=>1, 'id_komunitas'=>$model->id_komunitas,'id_user'=>Yii::$app->user->identity->id]);
                       return Html::a('Set Admin', $url, ['title' => 'setadmin','class'=>$class]);
                   },
                ]
            ]

          ,
        ],
    ]); ?>
</div>
