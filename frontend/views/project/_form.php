<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
$target=ArrayHelper::map(\app\models\Target::find()->all(),'id','nama');
$query = 'SELECT k.id as id,k.nama as nama FROM komunitas k,komunitasmember km WHERE km.id_komunitas = k.id AND km.id_user=:id_user AND km.role=1';
$listkomunitas= Yii::$app->db->createCommand($query)
                ->bindValue(':id_user',Yii::$app->user->identity->id)
                ->queryAll();
$listkomunitasarray=ArrayHelper::map($listkomunitas,'id','nama');
?>

<div class="project-form">
  <div class="row">
  <div class="col-lg-8 boxform">
    <h1><p class="text-center">Form Pendaftaran Project</p></h1>
    <?php $form = ActiveForm::begin([
      'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Input Nama Project'])->label(false) ?>

    <?= $form->field($model, 'deskripsi')->widget(CKEditor::className(), [
        'options' => ['rows' => 6,'placeholder'=>'Deskripsi Project'],
        'preset' => 'basic'
    ]) ?>
    <div class="row">
      <div class="col-lg-6">
        <?= $form->field($model, 'foto')->fileInput(['class'=>'form form-inline'])->label('Gambar Project') ?>
      </div>
      <div class="col-lg-6">
        <?= $form->field($model, 'lokasi')->textInput(['placeholder'=>'Lokasi Project'])->label(false) ?>
      </div>
    </div>


    <div class="row">
      <div class="col-lg-6">
        <?= $form->field($model, 'id_target')->dropDownList($target,['prompt'=>'---Pilih Target---'])->label(false) ?>
      </div>
      <div class="col-lg-6">
        <?= $form->field($model, 'val_target')->textInput(['maxlength' => true,'placeholder'=>'Jumlah Target','type'=>'number'])->label(false) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <?php
        echo DatePicker::widget([
          'model' => $model,
          'attribute' => 'date_start',
          'dateFormat' => 'yyyy-MM-dd',
          'name'=> 'datestart',
          'options' => [
            'class' => 'form-control',
            'placeholder'=>'Tanggal Mulai Campaign',
            ]
          ]);

         ?>
      </div>
      <div class="col-lg-6">
        <?php
        echo DatePicker::widget([
          'model' => $model,
          'attribute' => 'date_end',
          'dateFormat' => 'yyyy-MM-dd',
          'name'=> 'dateend',
          'options' => [
            'class' => 'form-control',
            'placeholder'=>'Tanggal Selesai Campaign',
            ]
          ]);
         ?>
      </div>
    </div>
    <br>
    <?= $form->field($model, 'id_komunitas')->dropDownList($listkomunitasarray)->label(false) ?>

    <?php echo $form->field($model, 'id_admin_komunitas')
      ->hiddenInput(['value' => Yii::$app->user->identity->id])
      ->label(false);
      ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success buttonfull']) ?>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>
</div>
