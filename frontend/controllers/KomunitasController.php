<?php

namespace frontend\controllers;

use Yii;
use app\models\Komunitas;
use app\models\Komunitasmember;
use app\models\KomunitasSearch;
use app\models\KomunitasmemberSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * KomunitasController implements the CRUD actions for Komunitas model.
 */
class KomunitasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Komunitas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Komunitas::find()->all();

        return $this->render('index', [
            'model'=>$model,
        ]);
    }

    /**
     * Displays a single Komunitas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Komunitas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Komunitas();
        $komunitasmember = new Komunitasmember();
        $foto = UploadedFile::getInstance($model, 'foto');

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($foto)){
              $foto->saveAs(Yii::getAlias('@frontend/web/uploads/').'komunitas'.$model->nama.'.'. $foto->extension);
              $model->foto = 'komunitas'.$model->nama.'.'.$foto->extension;
              $model->save(FALSE);
            }
            $komunitasmember->id_komunitas = $model->id;
            $komunitasmember->id_user = $model->id_admin_komunitas;
            $komunitasmember->status = 1;
            $komunitasmember->role = 1;
            $komunitasmember->save();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionListmanagekomunitas($id)
    {
      $joinedcommunity = Yii::$app->db->createCommand('SELECT k.id, k.nama FROM komunitasmember km, komunitas k WHERE km.id_komunitas=k.id AND id_user=:id AND role=2')
           ->bindValue(':id', $id)
           ->queryAll();
      return $this->render('listmanagekomunitas',[
        'model'=>$joinedcommunity,
      ]);
    }

    public function actionManagekomunitas($id_komunitas,$id_user)
    {
      $searchModel = new KomunitasmemberSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      $dataProvider->query->andWhere('komunitasmember.id_komunitas = '.$id_komunitas);
      $model = $this->findModel($id_komunitas);
      return $this->render('managekomunitas', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          'model'=>$model,
          'id_komunitas'=>$id_komunitas,
      ]);
    }
    public function actionBroadcast($id_komunitas)
    {
      $model=new Komunitas();
      return $this->render('broadcast',[
        'model'=>$model,
        'id_komunitas'=>$id_komunitas,
      ]);
    }
    public function actionSendbroadcast($id_komunitas)
    {
      $data=Yii::$app->request->get();
      $model = Yii::$app->db->createCommand('SELECT k.id, km.id_user, u.contact FROM komunitasmember km, komunitas k,user u WHERE km.id_komunitas=k.id AND id_komunitas=:id_komunitas AND km.status=1 AND km.id_user = u.id')
           ->bindValue(':id_komunitas', $id_komunitas)
           ->queryAll();
      foreach($model as $models)
      {
        $nohp=$models['contact'];
        $messages=$data['Komunitas']['deskripsi'];
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.mainapi.net/smsnotification/1.0.0/messages",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"msisdn\"\r\n\r\n".$nohp."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\n".$messages."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer 66fa2015aeca846426549e3baaa6a5f7",
            "Cache-Control: no-cache",
            "Postman-Token: bb826273-2664-bfd3-d3a2-673f55e071d5",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $mes='';
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
      }
      return $this->redirect('index');
      /*
      */
    }

    public function actionKonfirmasi($id,$status,$id_komunitas,$id_user)
    {
      $model = new Komunitasmember();
      $model=$model->findOne($id);
      $model->status=$status;
      //role 0=non member,1=member,2=admin
      if($status==1)
      {
        $model->role=1;
      }else if($status==2){
        $model->role=0;
      }
      $model->save();
      return $this->redirect(['managekomunitas','id_komunitas'=>$id_komunitas,'id_user'=>$id_user]);
    }

    public function actionSetadmin($id,$status,$id_komunitas,$id_user)
    {
        $model = new Komunitasmember();
        $model = $model->findOne($id);
        $model->role=2;
        $model->save();
        return $this->redirect(['managekomunitas','id_komunitas'=>$id_komunitas,'id_user'=>$id_user]);
    }

    /**
     * Updates an existing Komunitas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $foto = UploadedFile::getInstance($model, 'foto');

        if ($model->load(Yii::$app->request->post())) {
            if (!empty($foto)) {
                  $foto->saveAs(Yii::getAlias('@frontend/web/uploads/') . 'komunitas'.$id.'.' . $foto->extension);
                  $model->foto = 'komunitas'.$model->id.'.' . $foto->extension;
                  $model->save(FALSE);
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Komunitas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Komunitas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Komunitas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Komunitas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
